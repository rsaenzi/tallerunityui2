﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformAndChairs : MonoBehaviour {

	public GameObject screenWin;
	bool collisionDetected = false;

	void OnCollisionEnter(Collision collision) {

		// Con estas lineas evitamos que este codigo se ejecute cada vez que una silla impacte la plataforma
		// No es necesario en sus juegos...
		if(collisionDetected == false) {
			collisionDetected = true;

			// Busco el nivel 1
			GameObject level1 = GameObject.Find("Level1(Clone)");

			// Destruimos el nivel 1
			GameObject.Destroy(level1);


			// Busco la pantalla ScreenGame
			GameObject screenGame = GameObject.Find("Canvas/ScreenGame(Clone)");

			// Elimino la pantalla ScreenStart
			GameObject.Destroy(screenGame);


			// Busco al GameObject Canvas
			GameObject canvas = GameObject.Find("Canvas");

			// Creo una copia del prefab ScreenWin
			GameObject.Instantiate(screenWin, canvas.transform);
		}
	}
}
