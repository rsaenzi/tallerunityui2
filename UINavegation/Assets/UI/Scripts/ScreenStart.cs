﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenStart : MonoBehaviour {

	public GameObject screenGame;
	public GameObject level1;

	public void GoToScreenGame(){

		// Busco la pantalla ScreenStart
		GameObject screenStart = GameObject.Find("Canvas/ScreenStart");

		// Elimino la pantalla ScreenStart
		GameObject.Destroy(screenStart);


		// Busco al GameObject Canvas
		GameObject canvas = GameObject.Find("Canvas");

		// Creo una copia del prefab ScreenGame
		GameObject.Instantiate(screenGame, canvas.transform);


		// Cargamos el primer nivel del juego
		GameObject.Instantiate(level1);
	}
}
